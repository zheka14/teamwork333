<?php 
	require($_SERVER['DOCUMENT_ROOT'] . "/configs/db.php");
?>


<!DOCTYPE html>
<html>
<head>
	<title>Task manager - Додати завдання</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
	<main>
		<header>
			<div>
				<a href="/logout.php">ВИХІД</a>
			</div>
			<div>
				<a href="main.php">ГОЛОВНА</a>
			</div>
<!-- <<<<<<< HEAD --> 
			<h1 style="margin-right: 8%">Додати нове завдання</h1>
<!-- =======
			<h1 style="margin-right: 23vw">Додати нове завдання</h1>
>>>>>>> 3869931292df0a72533b56d6b31e848ea2626b53 -->
		</header>

		<div class="main-area" style="justify-content: normal; padding-bottom: 2vw;">
			<form action="add-task.php" method="POST" style="margin-left: 4.5vw; margin-top: 2.5vw; width: 50vw">
				<label for="title">
					<b>Суть завдання</b>
				</label>
				<input type="text" name="title" placeholder="Напишіть тут його короткий опис." style="width: 50vw; margin-top: 1vw">
				<br><br><br>
				<label for="desc">
					<b>Пояснення</b>
				</label>
				<!-- я думаю, опис можна зробити або через input, або через textarea-->
				<textarea type="text" name="desc" placeholder="Якщо потрібно, напишіть тут його пояснення." style="width: 50vw; height: 23vw; margin-top: 1vw; resize: none;"></textarea>
				<br>
				<button  type="submit" class="add-task-btn">Додати</button>
			</form>
			<div class="misc" style="padding: 1.5vw; left: 2vw">
				<p>
					Тут ви можете додати завдання.<br><br>
					В суті завдання радимо писати основне.<br><br>
					А в поясненні можете описати все, що вам потрібно(навіть до найдрібніших деталей).<br>
				</p>
			</div>
		</div>		
	</main>

</body>
</html>