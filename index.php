<?php 
require($_SERVER['DOCUMENT_ROOT'] . "/configs/db.php");
?>


<html>
<head>
	<title>Task manager - Вхід</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>


<body style="background: url('images/1.jpg') no-repeat; background-size: 100% 110%;">
	<div id="base">
		<div>
			<button class="login">Вхід</button><span>/</span><button class="register">Реєстрація</button>
		</div>

		<?php 
			include ($_SERVER['DOCUMENT_ROOT'] . "/login.php");
		?>
		<form action="/" style="display: none" method="POST" id="loginForm">
				<label for="login">
					<b>Логін</b>
				</label>
				<input name="login" type="text" placeholder="Введіть свій логін сюди">

				<label for="password">
					<b>Пароль</b>
				</label>
				<input name="password" type="password" placeholder="Введіть пароль від свого обл. запису тут">
				<br>
				<br>
				<button type="submit" class="submit">Ввійти</button>
		</form>

		<form style="display: none" method="POST" id="registerForm" action="/add_programmer.php" required>
			<label for="name_user">
				<b>Ім'я користувача/Псевдонім</b>
			</label>
			<input name="name_user" type="text" placeholder="Оберіть собі псевдонім та введіть його" required>
			<br>
			<label for="login">
				<b>Логін</b>
			</label>
			<input name="login" type="text" placeholder="Введіть ваш логін" required>
			<br>
			<label for="email">
				<b>Пошта</b>
			</label>
			<input name="email" type="email" placeholder="Введіть свою пошту сюди" required>
			<br>
			<label for="password">
				<b>Пароль</b>
			</label>
			<input name="password" type="password" placeholder="Придумайте та введіть свій пароль тут" required>
			<br>
			<br>
			<button type="submit" class="submit">Зареєструватися</button>
		</form>

	</div>

	<script type="text/javascript" src="js/login-page-script.js"></script>
</body>
</html>